---
title: About Ayoung
image: /images/built-by.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - ayoung
date: Tue Aug 13 2019 17:50:55 GMT+0100 (IST)
tags:
---

[Ayoung](https://twitter.com/saingsab) is a Co-founder of Bitriel, is a Decentralization Enthusiast and very passionate about Blockchain, FinTech and Cyber Security.

With three years background in Banking and Financial Institution as IT who responsible for Infra Structure and Payment Management System he has designed, Implemented and Developing both simple and sophisticated solutions of related industrial in Cambodia.

Because he love to solve the problem, In 2018 he co-founded Bitriel, a Cryptocurrency Exchange, Professional OCT service and Global Remittance where people can trade any cryptocurrency to cash, cryptocurrency brokerage firm and Inflow remittance from any country.

Beyond Bitreil, Saing is a Lead Technical Security of Koompi, a local branded and affordable laptop for next generation of engineer and problem solver.

In addition, he is a independent member of Information Systems Security Alliance (ISSA), a local information security community in Cambodia.

2018, Saing joint SmallWorld’s REDD lab a Research and Development which is focus on blockchain and decentralized application.
