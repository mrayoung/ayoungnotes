---
title: Blockchain and Cryptocurrency Updates 30-01-2020 | Bitcoin Price | Jae Kwon | Ditto Music | Avast
image: /images/20200130_080152.png
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - ayoung
date: Thu Jan 30 2020 7:06:59 GMT +07:00
tags:
  - cryptocurrency
---
<iframe width="700" height="392" src="https://www.youtube.com/embed/Hk3I5tEBsac" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Today Bitcoin price is 9241 USD trading BTC/USD It's still below the first resistance level (as long as its bellow 9400 USD) from my chart. If its pass the first resistant level then the next one is 1300USD per BTC.

Cosmos founder Jae Kwon is stepping down, The blockchain founder is going to work on open-collaboration technology, for a new project called Virgo.

Ed Sheeran joint Bluebox, from Ditto Music, Using blockchain technology to split royalties between artists.

PCMag and Motherboard published yesterday revealed that antivirus software Avast—which owns subsidiary AVG—has been and still is selling web browsing data of millions of its users to large companies such as Home Depot, Google, Microsoft, Pepsi, and others.

Thank you for watching Hit subscribe for supporting and get the latest update!

Other Channel
Lbry: https://lbry.tv/@ayoung:4
D.tube: https://d.tube/#!/c/saing