---
title: Docker Container For Developer 101 | Introduction to Docker
image: /images/20200601_01.png
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - ayoung
date: Mon JUN 01 2020 12:10:59 GMT +07:00
tags:
  - devop
---

Docker is an open platform that allows you to pack, develop, run, and ship applications in environments described containers. 

Throughout the past few years, Docker has radically changed the aspect of software engineering management. For developers, Docker is a must-know. For enterprises, Docker is a must-use.

Containers are innovative because they allow you as the developer to work with applications out of the trouble of installing extra dependencies. 

Docker your coding environment will promptly set up. you can spend your time sharpening on the code that truly matters to you, your team, and your business.

This two-part of video tutorials I will show you how to set up and solve the problem along the way from preparation to docker-compose 

Hope you enjoy it. 

Part II

<iframe width="700" height="392" src="https://www.youtube.com/embed/C-IbInJNkrs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Part II

<iframe width="700" height="315" src="https://www.youtube.com/embed/TJTWjzFt-CI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Lbry: https://lbry.tv/@ayoung:4 
D.tube: https://d.tube/#!/c/saing
