---
title: How to install OpenJDK In Pionux or any Arch Linux base
image: /images/OpenJDK.png
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - ayoung
date: Fri Sep 13 2019 15:06:59 GMT +07:00
tags:
  - how-to
---

### Introduction

JDK (Java Development Kit) is a software development environment used in Java platform programming. It contains many tools than the standalone JRE (Java Runtime Environment) as well as the other components needed for developing Java applications.

OpenJDK is free and open-source of Java SE Platform Edition. It started since 2007 that initial development of Sun Microsystems in 2006.

Tomcat, Jetty, Glassfish, Cassandra and Jenkins these software mainly required Java and JVM (Java’s virtual machine) environment to sit on top for its operation. 

In this guide, I will install Java Runtime Environment (JRE) and the Java Developer Kit (JDK) in Pionux and this method compatible with any archlinux base.

### Prerequisites

To follow this tutorial, you will need:

   - One Pionux or ArchLinux latest operating system following the [Pionux setup guide tutorial](https://youtu.be/1NZ4XcFevpA).

### Installing Default Versions of OpenJDK

By default, if you don't specify the version of your OpenJDK it's will lead you the latest version of OpenJDK, But you can also install different versions of OpenJDK as well.

To install this version, first update the package index:
```sh
$ sudo pacman -Syu
```
If you run the latest update you should see as below:

```sh
:: Synchronizing package databases...
 testing is up to date
 core is up to date
 extra is up to date
 community-testing is up to date
 community is up to date
 multilib-testing is up to date
 multilib is up to date
 archlinuxfr is up to date
:: Starting full system upgrade...
 there is nothing to do
```
Execute the following command to install OpenJDK:

```sh
 $sudo pacman -S jdk-openjdk

 Packages (6) java-environment-common-3-1  java-runtime-common-3-1  jre-openjdk-12.0.2.u10-1
             jre-openjdk-headless-12.0.2.u10-1  libnet-1.1.6-3  jdk-openjdk-12.0.2.u10-1

Total Download Size:   115.41 MiB
Total Installed Size:  267.77 MiB

:: Proceed with installation? [Y/n] 
```
Press Enter to confirm and proceed installation

```sh
:: Retrieving packages...
 java-runtime-common-3-1-any                       4.7 KiB  0.00B/s 00:00 [##########################################] 100%
 libnet-1.1.6-3-x86_64                            91.8 KiB   638K/s 00:00 [##########################################] 100%
 jre-openjdk-headless-12.0.2.u10-1-x86_64         35.5 MiB  2.23M/s 00:16 [##########################################] 100%
 jre-openjdk-12.0.2.u10-1-x86_64                 175.2 KiB  2.34M/s 00:00 [##########################################] 100%
 java-environment-common-3-1-any                   2.4 KiB  0.00B/s 00:00 [##########################################] 100%
 jdk-openjdk-12.0.2.u10-1-x86_64                  79.7 MiB  2.73M/s 00:29 [##########################################] 100%

```
During installation, you will be asked to enter the password of the sudo user. Enter the password and wait for a few moments to complete the installation.

That’s it. Now, check the java version using the bellow command:

```sh
$ java -version
```

Sample output:

```sh
openjdk version "12.0.2" 2019-07-16
OpenJDK Runtime Environment (build 12.0.2+10)
OpenJDK 64-Bit Server VM (build 12.0.2+10, mixed mode)
```
### Conclusion

In this tutorial, you installed the latest versions of OpenJDK. You can now install software which runs on Java, such as Tomcat, Jetty, Glassfish, Cassandra or Jenkins.

At the time of writing openjdk version "12.0.2" is the latest version. I do recommend to install the long term support version(LTS) which is the latest stable version.