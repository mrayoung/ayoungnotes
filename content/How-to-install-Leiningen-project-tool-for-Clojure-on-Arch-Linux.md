---
title: How to install Leiningen project tool for Clojure on Arch Linux
image: /images/leiningenandclojure.png
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - ayoung
date: Fri Sep 13 2019 18:54:59 GMT +07:00
tags:
  - how-to
---

### Introduction

Leiningen is a project tool for Clojure. Leiningen is a wide range use in Clojure projects. All my Clojure projects are using Leiningen.

Leiningen with its powerful command-line is run using the lein command. 

In this installation guide, we will install the lein command into our computer operating path, The correct path will enable us to use the lein command in any directory.

### Prerequisites

To follow this tutorial, you will need:

   - One Pionux or ArchLinux latest operating system following the [Pionux setup guide tutorial](https://youtu.be/1NZ4XcFevpA).
   - OpenJDK last stable version following [How to install OpenJDK In Pionux or any Arch Linux base](https://www.ayoungnotes.com/how-to-install-openjdk-in-pionux-or-any-arch-linux-base)

### Install lein

To install lein, execute the following commands in your terminal.

1. Download the lein script
```sh
$curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > lein
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 12537  100 12537    0     0  14714      0 --:--:-- --:--:-- --:--:-- 14697
```
2. Move the lein script to the user programs directory

```sh
$sudo mv lein /usr/local/bin/lein
```
3. Add execute permissions to the lein script

```sh
$sudo chmod a+x /usr/local/bin/lein
```

4. Verify your installation

Run the following command to verify our installation.

```sh
$lein version
```

For the first time you should take some time and finally you will see as below:

```sh
$~ lein version
Downloading Leiningen to /home/ayoung/.lein/self-installs/leiningen-2.9.1-standalone.jar now...
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   618    0   618    0     0    261      0 --:--:--  0:00:02 --:--:--   260
100 13.9M  100 13.9M    0     0   127k      0  0:01:51  0:01:51 --:--:--  304k
Leiningen 2.9.1 on Java 12.0.2 OpenJDK 64-Bit Server VM
```

After a while to run, It downloaded some resources. If it completes successfully, you are there!

### Get Started With Repl
execute bellow command 

```sh
$~ lein repl
```

Then it's will download some dependency from officials clojure and return as bellow:

```sh
Clojure 1.10.0
OpenJDK 64-Bit Server VM 12.0.2+10
    Docs: (doc function-name-here)
          (find-doc "part-of-name-here")
  Source: (source function-name-here)
 Javadoc: (javadoc java-object-or-class-here)
    Exit: Control+D or (exit) or (quit)
 Results: Stored in vars *1, *2, *3, an exception in *e

user=> 
```

Let's start writing one line of code to return the string

```sh
user=> (println "Hello World!")
Hello World!
nil
user=> 
```

### Conclusion
Clojure is a dynamic, general-purpose programming language, combining the approachability and interactive development of a scripting language with an efficient and robust infrastructure for multithreaded programming. [READ MORE](https://clojure.org)

[REF](https://purelyfunctional.tv/guide/how-to-install-clojure)