---
title: My Personal 2019 breakup challenge
image: /images/ayoung-with-bike.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - ayoung
date: Wed Jan 01 2020 13:06:59 GMT +07:00
tags:
  - life
---

2019 was a great challenge for me. despite to earn what I have done was to be less.
These are the things I have done:

1. Personal health: From several times in the hospital, I got Zero and Zero tablets of medicine. to achieve this I have engaged myself with sport and physical training, I join a bicycle team, playing football, and running, etc.

2. Less plastic: Owning a bottle and bringing it everywhere I go. Trying to say no when buying some stuff in the market. Stop owning a vehicle since 15 March 2019 owning a bike instead(still miss the off-road with my bike motorbike haha).

3. Sleep: I've slept less by understanding the restfulness of my body gives me enough time to rest instead of sleeping.

4. Food: Understand my body type and what food does its needs. eating less but fulfilled the need of the body. For coffee and energy drink cutting close to zero.

5. Speak: Not to response immediately try to listen more and understand the speaker thought (maybe sometimes I talk more but less if compared to last year loz).

I hope whoever reads this not think about my boastful healthy record. but the secret of living a healthy is not the amount of what you trying to fill in. What I want to share is instead of looking for more things to come in life may be there things that should have to cut off.
